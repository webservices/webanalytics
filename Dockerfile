FROM matomo:5.2.2-apache

# Check https://github.com/digitalist-se/extratools/tags for newer releases
ENV \
  PLUGIN_LOGVIEWER_VERSION=5.x-dev \
  PLUGIN_LOGINOIDC_VERSION=5.x-dev \
  PLUGIN_LICENCE_KEY=632bdc409d30ec72146aecba1197de4500296d2dbe3f9aaaaefd7403a387 \
  MATOMO_CORE_VERSION=5.2.2

RUN apt update && apt install -y unzip nano vim wget zip git rsync default-mysql-client gettext moreutils

#RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/src/matomo --filename=composer

RUN curl -L -o LogViewer.zip \
    https://github.com/matomo-org/plugin-LogViewer/archive/refs/heads/${PLUGIN_LOGVIEWER_VERSION}.zip \
    && unzip LogViewer.zip \
    && rm LogViewer.zip \
    && mv plugin-LogViewer-${PLUGIN_LOGVIEWER_VERSION} /usr/src/matomo/plugins/LogViewer

RUN curl -L -o LoginOIDC.zip \
    https://github.com/dominik-th/matomo-plugin-LoginOIDC/archive/refs/heads/${PLUGIN_LOGINOIDC_VERSION}.zip \
    && unzip LoginOIDC.zip \
    && rm LoginOIDC.zip \
    && mv matomo-plugin-LoginOIDC-${PLUGIN_LOGINOIDC_VERSION} /usr/src/matomo/plugins/LoginOIDC && \
    # Replace LoginOIDC defaults wth CERN ones
    sed -i "s|OAuth login|CERN OAuth login|" /usr/src/matomo/plugins/LoginOIDC/SystemSettings.php && \
    sed -i "s|https://github.com/login/oauth/authorize|https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/auth|" /usr/src/matomo/plugins/LoginOIDC/SystemSettings.php && \
    sed -i "s|https://github.com/login/oauth/access_token|https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/token|" /usr/src/matomo/plugins/LoginOIDC/SystemSettings.php && \
    sed -i "s|https://api.github.com/user|https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/userinfo|" /usr/src/matomo/plugins/LoginOIDC/SystemSettings.php && \
    sed -i 's/"id"/"sub"/' /usr/src/matomo/plugins/LoginOIDC/SystemSettings.php
    
RUN curl -o EnvironmentVariables.zip \
    https://plugins.matomo.org/api/2.0/plugins/EnvironmentVariables/download/latest \
    && unzip EnvironmentVariables.zip \
    && rm EnvironmentVariables.zip \
    && mv EnvironmentVariables /usr/src/matomo/plugins

RUN curl -o CustomVariables.zip \
    https://plugins.matomo.org/api/2.0/plugins/CustomVariables/download/latest \
    && unzip CustomVariables.zip \
    && rm CustomVariables.zip \
    && mv CustomVariables /usr/src/matomo/plugins

RUN git clone https://github.com/nodeone/extratools.git ExtraTools \
    && mv ExtraTools /usr/src/matomo/plugins

RUN curl -o LoginTokenAuth.zip \
    https://plugins.matomo.org/api/2.0/plugins/LoginTokenAuth/download/latest \
    && unzip LoginTokenAuth.zip \
    && rm LoginTokenAuth.zip \
    && mv LoginTokenAuth /usr/src/matomo/plugins

RUN curl -f -o CustomReports.zip -sS --data "access_token=fc8523d6d105d870df2e52bdaffd9b57bdf19aac35e7c0a312debaec7d73" https://plugins.matomo.org/api/2.0/plugins/CustomReports/download/latest?matomo=${MATOMO_CORE_VERSION} \
    && unzip  CustomReports.zip \
    && rm  CustomReports.zip \
    && mv  CustomReports  /usr/src/matomo/plugins

RUN month=$(date +"%m") \
    && year=$(date +"%Y") \
    && FILE=dbip-city-lite-$year-$month.mmdb.gz \
    && curl -o DBIP-City.mmdb.gz https://download.db-ip.com/free/$FILE \
    && gunzip DBIP-City.mmdb.gz \
    && mv  DBIP-City.mmdb /usr/src/matomo/misc

# Increase the php script timeout. The default is 30 seconds
RUN sed -i "s|max_execution_time = 30|max_execution_time = 360|g" /usr/local/etc/php/php.ini-production && \
    cp /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini

#Copy over the apache config files
COPY apache_conf/httpdcern10.conf /etc/apache2/conf-available/httpdcern10.conf 

# Change permissions for the /etc/apache2 directory to be able to edit configuration during initialization
RUN chmod -R 777 /etc/apache2

# Set Listen to 8080 as openshift wont allow listening from port 80 and set ServerName
RUN sed -i "s|Listen 80|Listen 8080|g" /etc/apache2/ports.conf && \
    ln -s /etc/apache2/conf-available/httpdcern10.conf /etc/apache2/conf-enabled/httpdcern10.conf && \
    ln -s /etc/apache2/mods-available/headers.load /etc/apache2/mods-enabled/headers.load && \
    ln -s /etc/apache2/mods-available/log_debug.load /etc/apache2/mods-enabled/log_debug.load

#
# Setup Matomo - copy Matomo files over /var/www/html
#
#RUN  rsync -ah  --exclude 'config/config.ini.php' /usr/src/matomo/* /var/www/html && \
RUN tar cf - --one-file-system -C /usr/src/matomo . | tar xf - && \
    chgrp -R 0 /var/www/html && chmod -R g=u /var/www/html

# Copy run scripts for both Apache and DB migration
#
COPY docker-entrypoint.sh init-container.sh /
RUN chmod +x /docker-entrypoint.sh /init-container.sh

EXPOSE 8080

ENTRYPOINT ["/docker-entrypoint.sh"]
