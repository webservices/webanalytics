#!/bin/sh
set -e

sed "s/APPLICATION_NAME/${APPLICATION_NAME}/g" -i  /etc/apache2/conf-available/httpdcern10.conf

# Replace LoginOIDC defaults wth CERN ones
# sed -i "s|OAuth login|CERN OAuth login|" /var/www/html/plugins/LoginOIDC/SystemSettings.php
# sed -i "s|https://github.com/login/oauth/authorize|https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/auth|" /var/www/html/plugins/LoginOIDC/SystemSettings.php
# sed -i "s|https://github.com/login/oauth/access_token|https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/token|" /var/www/html/plugins/LoginOIDC/SystemSettings.php
# sed -i "s|https://api.github.com/user|https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/userinfo|" /var/www/html/plugins/LoginOIDC/SystemSettings.php
# sed -i 's/"id"/"sub"/' /var/www/html/plugins/LoginOIDC/SystemSettings.php

# Config files from an Upgrade. Without this there are filesystem check errors
echo "Copy the config/ files into /var/www/html/config"
rsync -rlgoDv  --delete --exclude 'config.ini.php' --exclude '.htaccess' /usr/src/matomo/config/ /var/www/html/config

# Post installation
echo "Post installation: Commands to run after the initial setup and the Database has been configured "
echo "Dont use these commands until the initial setup has finished and the config/config.ini.php file exists"
echo "cd /var/www/html"
echo "php console config:set 'mail.transport=\"smtp\"' 'mail.port=\"25\"' 'mail.host=\"cernmx.cern.ch\"' 'mail.encryption=\"tls\"' 'noreply_email_address = \"matomo-service-admins@cern.ch\"' 'noreply_email_name = \"matomo-service-admins@cern.ch\"'"
echo "php console config:set 'General.force_ssl=1'"
echo "php console config:set 'log.log_writers[]=\"file\"' 'log.log_level=\"DEBUG\"'"

# Active the plugins
echo "php console plugin:activate LogViewer"
echo "php console plugin:activate LoginOIDC"
echo "php console plugin:activate EnvironmentVariables"

# Configure archiving
echo "Enable archiving through cronjob via the General settings"

echo "Show variables"
echo ${MATOMO_DATABASE_HOST}
echo $MATOMO_DATABASE_USERNAME
echo $MATOMO_DATABASE_DBNAME

echo "Start apache in the foreground"
apache2-foreground
