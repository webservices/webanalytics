#!/bin/bash
set -e

#Config files from an Upgrade. Without this there are filesystem check errors
# This needs to be done on both init & actual container, as the files to need be of new version
echo "Copy the config/ files into /var/www/html/config"
rsync -rlgoDv  --delete --exclude 'config.ini.php' --exclude '.htaccess' /usr/src/matomo/config/ /var/www/html/config

# Update Matomo database in case there is a new version.
if [ -f /var/www/html/config/config.ini.php ];
then php console core:update --yes;
php console core:create-security-files;
fi
